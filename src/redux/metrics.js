import moment from 'moment';

let week = []

for (let i = 6; i >= 0; i--) {
    week.push(moment().subtract(i,'d').format('YYYY/MM/DD'));
}

console.log(week)

const initialState = {
    week: week,
    traffic: {
        direct: [234, 134, 354, 55, 289, 432, 344],
        email: [113, 143, 80, 143, 149, 114, 188],
        social: [11, 14, 8, 14, 14, 11, 18]
    },
    funnel: [
        ['Website Visits', 43434],
        ['Trial Started', 7034],
        ['Accounts Linked', 1987],
        ['Budget Created', 1176],
        ['Subscribed', 947]
    ],
    subscription: {
        new: [234, 134, 354, 55, 289, 432, 344],
        recurring: [113, 143, 80, 143, 149, 114, 188],
        cancelled: [11, 14, 8, 14, 14, 11, 18]
    }
}

export default function metrics (state = initialState, action) {
    switch (action.type) {
        default :
            return state
    }
}
