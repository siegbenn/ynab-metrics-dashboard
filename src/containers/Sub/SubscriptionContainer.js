import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactHighcharts from 'react-highcharts';

class GraphContainer extends Component {

    render() {
        let subscriptionConfig = {
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Subscription Report'
            },
            legend: {
                layout: 'horizontal',
                align: 'left',
                verticalAlign: 'top',
                x: 150,
                y: 50,
                floating: true,
                borderWidth: 2
            },
            xAxis: {
                categories: this.props.week
            },
            yAxis: {
                title: {
                    text: 'Subscriptions'
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: ' Subscriptions'
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'New',
                data: this.props.subscription.new,
                color: '#00b19d'
            },{
                name: 'Recurring',
                data: this.props.subscription.recurring,
                color: '#0194C8'
            },{
                name: 'Cancelled',
                data: this.props.subscription.cancelled,
                color: '#ef5350'
            }]
        }
      
        return (
            <div className="col-sm-12">
                <div className="card-box">
                    <div className="widget-chart text-center">
                        <ReactHighcharts config={subscriptionConfig}></ReactHighcharts>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps({ metrics }) {
    return {
        subscription: metrics.subscription,
        week: metrics.week
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GraphContainer)