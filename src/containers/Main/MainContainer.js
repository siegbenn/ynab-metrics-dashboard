import React, { Component } from 'react';
import { NavigationContainer } from '..';
import { Footer } from '../../components';

class MainContainer extends Component {
    render() {

        return (
            <div>
                <NavigationContainer />
                <div className="wrapper">
                    <div className="container">
                        {this.props.children}
                        <Footer />
                    </div>
                </div>
            </div>
        )
    }
}

export default MainContainer;
