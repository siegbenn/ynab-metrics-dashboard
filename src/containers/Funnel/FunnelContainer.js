import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactHighcharts from 'react-highcharts';
require('highcharts-funnel')(ReactHighcharts.Highcharts)

class FunnelContainer extends Component {

    render() {
        
        let funnelConfig = {
            chart: {
                type: 'funnel',
                marginRight: 100
            },
            title: {
                text: 'Acquisition Funnel',
                x: -50
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b> ({point.y:,.0f})',
                        color: 'black',
                        softConnector: true
                    },
                    neckWidth: '30%',
                    neckHeight: '25%'
                }
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Acquisition Funnel',
                data: this.props.funnel
            }]
        }

        return (
            <div className="col-lg-5">
                <div className="card-box">
                    <ReactHighcharts config={funnelConfig}></ReactHighcharts>
                </div>
            </div>
        );
    }
}

function mapStateToProps({ metrics }) {
    return {
        funnel: metrics.funnel,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FunnelContainer)