import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactHighcharts from 'react-highcharts';

class TrafficContainer extends Component {

    render() {

        let trafficConfig = {
            chart: {
                type: 'area'
            },
            title: {
                text: 'Traffic Source'
            },
            xAxis: {
                categories: this.props.week,
                tickmarkPlacement: 'on',
                title: {
                    enabled: false
                }
            },
            yAxis: {
                title: {
                    text: 'Percent'
                }
            },
            credits: {
                enabled: false
            },
            colors: ['#00b19d', '#0194C8', '#ef5350'],
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b><br/>',
                split: true
            },
            plotOptions: {
                area: {
                    stacking: 'percent',
                    lineColor: '#ffffff',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#ffffff'
                    }
                }
            },
            series: [{
                name: 'Direct',
                data: this.props.traffic.direct
            }, {
                name: 'Email',
                data: this.props.traffic.email
            }, {
                name: 'Social Media',
                data: this.props.traffic.social
            }]
        }

        return (
            <div className="col-lg-7">
                <div className="card-box">
                    <ReactHighcharts config={trafficConfig}></ReactHighcharts>
                </div>
            </div>            
        );
    }
}

function mapStateToProps({ metrics }) {
    return {
        traffic: metrics.traffic,
        week: metrics.week
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TrafficContainer)