// React
import React from 'react'
import ReactDOM from 'react-dom'

// Redux
import * as reducers from './redux'
import { createStore, compose, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

// Routing
import getRoutes from './config/routes'
import { browserHistory } from 'react-router'
import { routerReducer, syncHistoryWithStore } from 'react-router-redux'

// Initialize Redux store with middleware.
const store = createStore(combineReducers({
    ...reducers,
    routing: routerReducer,
}), compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : (f) => f
))

const history = syncHistoryWithStore(browserHistory, store)

ReactDOM.render(
    <Provider store={store}>
        {getRoutes(history)}
    </Provider>,
    document.getElementById('root')
)