import React from 'react';
import { TrafficContainer, FunnelContainer, SubscriptionContainer } from '../../containers';

const Home = (props) => {
    
    return (
        <div>
            <div className="row">
                <div className="col-sm-12">
                    <div className="page-title-box">
                        <h4 className="page-title"><u>Seven Day Summary</u></h4>
                    </div>
                </div>
            </div>
            <div className="row">
                 <SubscriptionContainer />
            </div>
            <div className="row">
                <FunnelContainer />
                <TrafficContainer />
            </div>            
        </div>
    )
}

export default Home;
