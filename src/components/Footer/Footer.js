import React, { Component } from 'react';

class Footer extends Component {

    render() {
        return (
            <footer className="footer text-right">
              <div className="container">
                  <div className="row">
                      <div className="col-xs-12 text-center">
                          YNAB Metrics Dashboard
                      </div>
                </div>
              </div>
            </footer>
        )
    };
}

export default Footer;
