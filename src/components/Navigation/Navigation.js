import React, { Component } from 'react';

class Navigation extends Component {

    render() {
      return (
          <header id="topnav">
            <div className="topbar-main">
              <div className="container">
                <div className="logo">
                  <p className="logo"><img alt="WE Logo" src="/assets/images/ynab-mark.svg" height="32" /><span> YNAB Metrics Dashboard</span></p>
              </div>
            </div>
          </div>
        </header>
      )
    };
}


export default Navigation;
