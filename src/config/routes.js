import React from 'react'
import { Router, Route, IndexRoute } from 'react-router'
import {
    HomeContainer,
    MainContainer,
    NotFoundContainer
} from '../containers'

export default function getRoutes(history) {
    return (
        <Router history={history}>
            <Route path='/' component={MainContainer}>
                <IndexRoute component={HomeContainer} />
            </Route>
            <Route path='*' component={NotFoundContainer} />
        </Router>
    )
}
